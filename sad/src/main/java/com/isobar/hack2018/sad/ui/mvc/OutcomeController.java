package com.isobar.hack2018.sad.ui.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author bwynn
 */
@Controller
@RequestMapping("/outcomes")
public class OutcomeController {

	@RequestMapping
	public ModelAndView index() {
		return new ModelAndView("redirect:/lookup");
	}

	@RequestMapping("{type}")
	public ModelAndView outcome(@PathVariable("type") String type) {
		return new ModelAndView("pages/outcomes/" + type);
	}

}
