package com.isobar.hack2018.sad.ui.mvc;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.isobar.hack2018.sad.ml.SADIndexCache;
import com.isobar.hack2018.sad.model.LookupForm;
import com.isobar.hack2018.sad.model.SADIndexResult;

/**
 * @author bwynn
 */
@Controller
@RequestMapping("/lookup")
public class LookupController {
	
	private static final Logger LOG = LoggerFactory.getLogger(LookupController.class);
	
	private static final Float VAL_NEUTRAL = 0.2f;
	
	@Autowired
	private SADIndexCache sadIndexCache;

	@RequestMapping
	public ModelAndView lookup() {
		Map<String, Object> model = new HashMap<>();
		model.put("lookupForm", new LookupForm());
		
		return new ModelAndView("pages/lookup", model);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView create(@Valid @ModelAttribute("lookupForm") LookupForm lookupForm, BindingResult result, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			return new ModelAndView("pages/lookup", "formErrors", result.getAllErrors());
		}
		redirect.addFlashAttribute("globalMessage", "So you're in "+lookupForm.getZipcode()+" i see!");
		return findOutcome(lookupForm.getZipcode());
	}
	
	@RequestMapping("{zipcode}")
	public ModelAndView list(@PathVariable("zipcode") String zipcode) {
		return findOutcome(zipcode);

	}

	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(@ModelAttribute String message) {
		return "pages/form";
	}
	
	
	public ModelAndView findOutcome(String zipcode) {
		SADIndexResult sadIndex = sadIndexCache.getIndexByZipcode(zipcode);
		if (sadIndex == null) {
			LOG.warn("no Index result");
			return new ModelAndView("redirect:/outcomes/failure");
		}
		
		Float value = sadIndex.getValue();
		if (value == null) {
			LOG.warn("index result has no value");
			return new ModelAndView("redirect:/outcomes/failure");
		}

		if (value > VAL_NEUTRAL) {
			// Happy Day
			LOG.info("happy day: {} above {}", value, VAL_NEUTRAL);
			return new ModelAndView("redirect:/outcomes/outcome-1");
		}
		
		if (value < -VAL_NEUTRAL) {
			// Bad End
			LOG.info("bad end: {} below -{}", value, VAL_NEUTRAL);
			return new ModelAndView("redirect:/outcomes/outcome-3");
		}
		
		// Neutral
		LOG.info("default route: {} between +/- {}", value, VAL_NEUTRAL);
		return new ModelAndView("redirect:/outcomes/outcome-2");
	}


}
