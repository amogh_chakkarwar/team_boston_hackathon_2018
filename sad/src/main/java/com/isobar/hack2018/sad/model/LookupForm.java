package com.isobar.hack2018.sad.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LookupForm {

	@Pattern(regexp="[0-9]*")
	@NotNull
	@Size(min=5, max=5)
	private String zipcode;
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public String getZipcode() {
		return zipcode;
	}
}
