package com.isobar.hack2018.sad.weather;

import java.io.Serializable;

public class WeatherOutput implements Serializable {

    private Query query;

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public class Query {
        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        int count;

        private Results results;

        public Results getResults() {
            return results;
        }

        public void setResults(Results results) {
            this.results = results;
        }

        public class Results {

            private Channel channel;

            public Channel getChannel() {
                return channel;
            }

            public void setChannel(Channel channel) {
                this.channel = channel;
            }

            public class Channel {

                private Location location;

                private Item item;

                private Astronomy astronomy;

                public Location getLocation() {
                    return location;
                }

                public void setLocation(Location location) {
                    this.location = location;
                }

                public Item getItem() {
                    return item;
                }

                public void setItem(Item item) {
                    this.item = item;
                }

                public Astronomy getAstronomy() {
                    return astronomy;
                }

                public void setAstronomy(Astronomy astronomy) {
                    this.astronomy = astronomy;
                }

                public class Location {

                    private String city;
                    private String country;
                    private String region;

                    public String getCity() {
                        return city;
                    }

                    public void setCity(String city) {
                        this.city = city;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getRegion() {
                        return region;
                    }

                    public void setRegion(String region) {
                        this.region = region;
                    }
                }

                public class Item {

                    private Condition condition;

                    public Condition getCondition() {
                        return condition;
                    }

                    public void setCondition(Condition condition) {
                        this.condition = condition;
                    }

                    public class Condition {

                        private String temp;

                        private String text;

                        public String getTemp() {
                            return temp;
                        }

                        public void setTemp(String temp) {
                            this.temp = temp;
                        }

                        public String getText() {
                            return text;
                        }

                        public void setText(String text) {
                            this.text = text;
                        }
                    }
                }

                public class Astronomy {
                    private String sunrise;
                    private String sunset;

                    public String getSunrise() {
                        return sunrise;
                    }

                    public void setSunrise(String sunrise) {
                        this.sunrise = sunrise;
                    }

                    public String getSunset() {
                        return sunset;
                    }

                    public void setSunset(String sunset) {
                        this.sunset = sunset;
                    }
                }

            }
        }
    }
}