package com.isobar.hack2018.sad.ml;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.machinelearning.AmazonMachineLearning;
import com.amazonaws.services.machinelearning.AmazonMachineLearningClientBuilder;
import com.amazonaws.services.machinelearning.model.PredictRequest;
import com.amazonaws.services.machinelearning.model.PredictResult;
import com.amazonaws.services.machinelearning.model.Prediction;
import com.isobar.hack2018.sad.model.SADIndexResult;
import com.isobar.hack2018.sad.model.Weather;

/**
 * Connects to AWS ML Model and finds a prediction
 * 
 * @author bwynn
 *
 */
public class SADIndexML {

	private static final Logger LOG = LoggerFactory.getLogger(SADIndexML.class);
	
	private static final String ML_MODEL = "ml-zktQZmmkzmQ";
	private static final String ML_ENDPOINT = "https://realtime.machinelearning.us-east-1.amazonaws.com/";
	/*
	 * {
    "MLModelId": "ml-9ILuHg5habt",
    "Record":{
"ZIP": "2110", 
"DAY_OF_YEAR":"44",
"PRECIP":"0.09",
"TEMP":"30",
"SUNRISE":"643",
"SUNSET":"1714",
"DAYLIGHT":"1071",
"SHORT_DAY":"0",
"HIGH_PRECIP":"0",
"COLD_DAY":"0",
"SINE_DAYLIGHT":"0.279328666"
},
    "PredictEndpoint": "https://realtime.machinelearning.us-east-1.amazonaws.com/ml-9ILuHg5habt"
}
	 */
	public SADIndexResult lookup(Weather weather) {
		Map<String, String> record = convertRecord(weather);
		if (record == null) {
			LOG.warn("no Weather");
			return null;
		}
		
		PredictRequest predictRequest = new PredictRequest()
				.withMLModelId(ML_MODEL)
				.withPredictEndpoint(ML_ENDPOINT);
		predictRequest.setRecord(record);
		
		AmazonMachineLearning client = getClient();
		
		PredictResult predictResult = client.predict(predictRequest);
		if (predictResult == null) {
			LOG.warn("no PredictResult");
			return null;
		}
		
		Prediction prediction = predictResult.getPrediction();
		if (prediction == null) {
			LOG.warn("no predicition");
			return null;
		}
		
		Float value = prediction.getPredictedValue();
		if (value == null) {
			LOG.warn("no value");
			return null;
		}
		
		LOG.info("returning new SADIndex of: " + value.toString());
		SADIndexResult sadIndex = new SADIndexResult();
		sadIndex.setValue(value);

		return sadIndex;
	}
	
	public AmazonMachineLearning getClient() {
		return AmazonMachineLearningClientBuilder.defaultClient();
	}
	
	/**
	 * Convert a Weather model into a ML Record request.
	 * @param weather
	 * @return
	 */
	public Map<String, String> convertRecord(Weather weather) {
		Map<String, String> record = new HashMap<>();
		
		if (weather == null) {
			return record;
		}
		
		if (weather.getZipcode() != null) {
			record.put("ZIP", weather.getZipcode());
		}
		
		if (weather.getDayOfYear() != null) {
			record.put("DAY_OF_YEAR", weather.getDayOfYear());
		}
		
		if (weather.getPrecip() != null) {
			record.put("PRECIP", weather.getPrecip());
		}
		
		if (weather.getTemp() != null) {
			record.put("TEMP", weather.getTemp());
		}
		
		if (weather.getSunrise() != null) {
			record.put("SUNRISE", weather.getSunrise());
		}
		
		if (weather.getSunset() != null) {
			record.put("SUNSET", weather.getSunset());
		}
		
		if (weather.getDaylight() != null) {
			record.put("DAYLIGHT", weather.getDaylight());
		}
		
		if (weather.getShortDay() != null) {
			record.put("SHORT_DAY", weather.getShortDay());
		}
		
		if (weather.getHighPrecip() != null) {
			record.put("HIGH_PRECIP", weather.getHighPrecip());
		}
		
		if (weather.getColdDay() != null) {
			record.put("COLD_DAY", weather.getColdDay());
		}
		
		if (weather.getSineDaylight() != null ) {
			record.put("SINE_DAYLIGHT", weather.getSineDaylight());
		}
		
		return record;
	}

}
