package com.isobar.hack2018.sad.model;

public class SADIndexResult {
	Float value;
	
	public void setValue(Float value) {
		this.value = value;
	}
	
	public Float getValue() {
		return value;
	}
	
}
