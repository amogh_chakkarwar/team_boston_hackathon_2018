package com.isobar.hack2018.sad.model;

public class Weather {
	private String coldDay;
	private String daylight;
	private String dayOfYear;
	private String highPrecip;
	private String precip;
	private String shortDay;
	private String sineDaylight;
	private String sunrise;
	private String sunset;
	private String temp;
	private String zipcode;
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getColdDay() {
		return coldDay;
	}
	
	public String getDaylight() {
		return daylight;
	}
	
	public String getDayOfYear() {
		return dayOfYear;
	}
	
	public String getHighPrecip() {
		return highPrecip;
	}
	
	public String getPrecip() {
		return precip;
	}
	
	public String getShortDay() {
		return shortDay;
	}
	
	public String getSineDaylight() {
		return sineDaylight;
	}
	
	public String getSunrise() {
		return sunrise;
	}
	
	public String getSunset() {
		return sunset;
	}
	
	public String getTemp() {
		return temp;
	}
	
	public String getZipcode() {
		return zipcode;
	}
	
	public void setColdDay(String coldDay) {
		this.coldDay = coldDay;
	}
	
	public void setDaylight(String daylight) {
		this.daylight = daylight;
	}
	
	public void setDayOfYear(String dayOfYear) {
		this.dayOfYear = dayOfYear;
	}
	
	public void setHighPrecip(String highPrecip) {
		this.highPrecip = highPrecip;
	}
	
	public void setPrecip(String precip) {
		this.precip = precip;
	}
	
	public void setShortDay(String shortDay) {
		this.shortDay = shortDay;
	}
	
	public void setSineDaylight(String sineDaylight) {
		this.sineDaylight = sineDaylight;
	}
	
	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}
	
	public void setSunset(String sunset) {
		this.sunset = sunset;
	}
	
	public void setTemp(String temp) {
		this.temp = temp;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
