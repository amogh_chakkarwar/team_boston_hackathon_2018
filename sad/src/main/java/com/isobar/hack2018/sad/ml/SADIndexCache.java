package com.isobar.hack2018.sad.ml;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.isobar.hack2018.sad.model.SADIndexResult;
import com.isobar.hack2018.sad.model.Weather;
import com.isobar.hack2018.sad.weather.WeatherCache;

public class SADIndexCache {

	private static final Logger LOG = LoggerFactory.getLogger(SADIndexCache.class);
	
	// TODO: timeout cache entries based on date?
	private static final Map<String, SADIndexResult> cache = new HashMap<>();
	
	
	@Autowired
	private WeatherCache weatherCache;
	
	@Autowired
	private SADIndexML sadIndexML;
	
	public SADIndexResult getIndexByZipcode(String zipcode) {
		if (zipcode == null || zipcode.length() < 5) {
			return null;
		}
		
		SADIndexResult result = cache.get(zipcode);
		if (result == null) {
			Weather weather = weatherCache.getWeatherByZipcode(zipcode);
			if (weather == null) {
				return null;
			}
			
			LOG.warn("sadIndex cache miss, looking up sadIndex for zipcode: {}", zipcode);
			result = sadIndexML.lookup(weather);
			cache.put(zipcode, result);
		} else {
			LOG.info("sadIndex cache hit, returning cached value for zipcode: {}", zipcode);
		}
		
		
		return result;
	}
}
