package com.isobar.hack2018.sad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.isobar.hack2018.sad.ml.SADIndexCache;
import com.isobar.hack2018.sad.ml.SADIndexML;
import com.isobar.hack2018.sad.weather.WeatherCache;
import com.isobar.hack2018.sad.weather.WeatherClient;

/**
 * 
 * @author bwynn
 *
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class SADApplication {

	@Bean
	public SADIndexML sadIndexML() {
		return new SADIndexML();
	}
	
	@Bean
	public SADIndexCache sadIndexCache() {
		return new SADIndexCache();
	}
	
	@Bean
	public WeatherCache weatherCache() {
		return new WeatherCache();
	}

	@Bean
	public WeatherClient weatherClient() {
		return new WeatherClient();
	}
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(SADApplication.class, args);
	}

}
