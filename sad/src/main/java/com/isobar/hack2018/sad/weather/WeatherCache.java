package com.isobar.hack2018.sad.weather;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.isobar.hack2018.sad.model.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import rx.Observable;

public class WeatherCache {

	private static final Map<String, Weather> cache = new HashMap<>();

	@Autowired
	private WeatherClient weatherClient;
	
	public Weather getWeatherByZipcode(String zipcode) {
		if (zipcode == null || zipcode.length() < 5) {
			return null;
		}
		
		Weather result = cache.get(zipcode);
		if (result == null) {
			result = getWeather(zipcode);
			if (result == null) {
				return null;
			}
			cache.put(zipcode, result);
		}
		
		return result;
	}
	
	public Weather getWeather(String zipcode) {

		String query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + zipcode + "\")";
		String format = "json";
		String env = "store://datatables.org/alltableswithkeys";

		Observable<WeatherOutput> weatherOutputObservable = weatherClient.getWeatherService().getWeather(query, format, env);
		WeatherOutput weatherOutput = weatherOutputObservable.toBlocking().first();

		SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");

	    SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
	    
		Weather weather = new Weather();
		Calendar calendar = Calendar.getInstance();
		try {
			String sunrise = date24Format.format(date12Format.parse(weatherOutput.getQuery().getResults().getChannel().getAstronomy().getSunrise()));
			String sunset = date24Format.format(date12Format.parse(weatherOutput.getQuery().getResults().getChannel().getAstronomy().getSunset()));
			int daylight = Integer.parseInt(sunset.replace(":", "")) - Integer.parseInt(sunrise.replace(":", ""));
			int temp = Integer.parseInt(weatherOutput.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
			int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR); 
			
			weather.setZipcode(zipcode);
			weather.setText(weatherOutput.getQuery().getResults().getChannel().getItem().getCondition().getText());//sunny/rainy/clear
			weather.setPrecip("");// unable to get this from web service
			weather.setDaylight(String.valueOf(daylight));
			weather.setSunrise(sunrise.replace(":", "").replaceFirst("^0+(?!$)", ""));
			weather.setSunset(sunset.replace(":", "").replaceFirst("^0+(?!$)", ""));
			weather.setTemp(weatherOutput.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
			weather.setDayOfYear(String.valueOf(dayOfYear));
			
			if(daylight<910) {
				weather.setShortDay("1");
			} else {
				weather.setShortDay("0");
			}
			
			weather.setHighPrecip("0");// unable to get this from web service
			
			if(temp <= 25) {
				weather.setColdDay("1");
			} else {
				weather.setColdDay("0");
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return weather;
	}
}
