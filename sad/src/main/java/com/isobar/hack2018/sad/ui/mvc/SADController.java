package com.isobar.hack2018.sad.ui.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author bwynn
 */
@Controller
@RequestMapping("/")
public class SADController {
	
	@RequestMapping
	public ModelAndView index() {
		return new ModelAndView("pages/index");
	}
	
}
