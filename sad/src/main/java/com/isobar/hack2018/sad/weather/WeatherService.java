package com.isobar.hack2018.sad.weather;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherService {

    @GET("/v1/public/yql")
    Observable<WeatherOutput> getWeather(@Query("q") String query, @Query("format") String format, @Query("env") String env);

}