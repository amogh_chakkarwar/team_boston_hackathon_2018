package com.isobar.hack2018.sad.weather;

import rx.Observable;

public class WeatherClientTester {

    private WeatherClient weatherClient;

    public void testWeatherClient() {
        String zipCode = "02110";
        if (weatherClient.getWeatherService() != null) {

            String query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + zipCode + "\")";
            String format = "json";
            String env = "store://datatables.org/alltableswithkeys";
            Observable<WeatherOutput> weatherOutputObservable = weatherClient.getWeatherService().getWeather(query, format, env);
            WeatherOutput weatherOutput = weatherOutputObservable.toBlocking().first();
            System.out.println(weatherOutput.getQuery().getResults().getChannel().getLocation().getCity());
            System.out.println(weatherOutput.getQuery().getResults().getChannel().getLocation().getRegion());
            System.out.println(weatherOutput.getQuery().getResults().getChannel().getLocation().getCountry());
            System.out.println(weatherOutput.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        }
    }
}