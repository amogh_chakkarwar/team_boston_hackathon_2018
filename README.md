# README #

### What is this repository for? ###

This repository is for the POC build for Team Boston Hackathon 2018
* Version: POC 

### How do I get set up? ###

* Summary of set up

checkout code from this repo
import maven project into eclipse.

* Configuration
* Dependencies

maven, eclipse

* Database configuration
* How to run 

mvn spring-boot:run

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
